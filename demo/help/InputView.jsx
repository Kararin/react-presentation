import React from 'react';
import {Input} from 'react-bootstrap';
import {GlyphButton} from 'GlyphButton';

export class InputView extends React.Component {
    constructor(props) {
        super(props);

        this.inputText = props.text;
    }

    render() {
        var button = <GlyphButton bsStyle = "success"
                                  glyph = "ok"
                                  clickCallback = {this.clickHandler.bind(this)}/>;
        return (<Input type = "text"
                       defaultValue = {this.props.text}
                       buttonAfter = {button}
                       onChange = {this.updateInputText.bind(this)}/>);
    }

    clickHandler() {
        this.props.clickCallback(this.inputText);
    }

    updateInputText(e) {
        e.preventDefault();
        this.inputText = e.target.value;
    }
}

InputView.propTypes = {
    text: React.PropTypes.string,
    clickCallback: React.PropTypes.func
};

InputView.defaultProps = {
    text: "",
    clickCallback: () => console.log('clickCallback was not defined')
};