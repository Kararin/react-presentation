import React from 'react';
import ReactDOM from 'react-dom';
import {MainView} from 'MainView';
import {InputView} from 'InputView';

document.addEventListener('DOMContentLoaded', () => {
    var el = document.getElementById('content');

    ReactDOM.render( <MainView/> , el);
});
