import React from 'react';
import {ButtonGroup, Col, Row} from 'react-bootstrap';
import {InputView} from 'InputView';
import {GlyphButton} from 'GlyphButton';

export class OneTaskView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            task: props.task
        };
    }

    render() {
        var content = this.getContent(),
            buttons = this.getButtons();

        return (
            <Row className = "show-grid">
                {content}
                {buttons}
            </Row>
        );
    }

    getContent() {
        var text = this.state.task.text,
            content = text;

        return  (<Col xs={4}> {content} </Col>);
    }

    getButtons() {
        var buttons = {
                delete: null,
                update: null
            },
            handlers = {
                delete: this.deleteClickHandler.bind(this),
                update: this.doneClickHandler.bind(this)
            },
            glyhps = {
                delete: 'remove',
                update: 'ok'
            },
            content = null;

            if (!this.props.editMode) {
                _.each(buttons, (value, key) => {
                    buttons[key] = (this.props[key])? <GlyphButton glyph = {glyhps[key]}
                                                                   clickCallback = {handlers[key]}/>: null
                });

                content = (
                    <Col xs={4}>
                        <ButtonGroup>
                            {buttons.delete}
                            {buttons.update}
                        </ButtonGroup>
                    </Col>
                );
            }

        return content;
    }

    deleteClickHandler() {
        this.props.deleteCallback(this.props.task);
    }

    doneClickHandler() {
        this.props.toDoneCallback(this.props.task);
    }
}

OneTaskView.propTypes = {
    task: React.PropTypes.object,
    delete: React.PropTypes.bool,
    update: React.PropTypes.bool
};

OneTaskView.defaultProps = {
    task: null,
    delete: false,
    update: false
};
