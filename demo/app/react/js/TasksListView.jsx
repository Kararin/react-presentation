import React from 'react';
import {Grid} from 'react-bootstrap';
import {OneTaskView} from 'OneTaskView';

export class TasksListView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tasks: []
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            tasks: nextProps.tasks
        });
    }

    render() {
        var gridProps = {
            delete: this.props.delete,
            update: this.props.update,
            edit: this.props.edit,
            deleteCallback: this.deleteTask.bind(this),
            toDoneCallback: this.props.toDoneCallback
        };

        return (
            <Grid>
                  {this.state.tasks.map((item) => <OneTaskView key = {item.id}
                                                               task = {item}
                                                               {...gridProps}/>)}
            </Grid>
        );
    }

    deleteTask(taskToDelete) {
        this.setState((state, props) => {
            state.tasks.splice(state.tasks.indexOf(taskToDelete), 1);
        });
    }
}

TasksListView.propTypes = {
    delete: React.PropTypes.bool,
    edit: React.PropTypes.bool,
    update: React.PropTypes.bool
};

TasksListView.defaultProps = {
    delete: true,
    update: false,
    edit: true
};

