export class TaskModel {
    constructor(text) {
        this.text = text;
        this.done = false;
        this.id = this.generateId();
    }

    generateId() {
        return new Date() - 1;
    }

    get isDone() {
        return this.done;
    }

    set isDone(toDone) {
        this.done = toDone;
    }

    set newText(text) {
        this.text = text;
    }
}