import React from 'react';
import {Input, Button} from 'react-bootstrap';

export class NewTask extends React.Component {
    constructor(props) {
        super(props);

        this.taskText = '';
    }

    render() {
        return (
            <div>
                <h3>
                    Add new Task
                </h3>
                <Input type = "text"
                       label = "Enter content"
                       onChange = {this.updateTaskText.bind(this)}/>

                <Button bsStyle = "primary" onClick = {this.clickHandler.bind(this)}>
                    Submit
                </Button>
            </div>
        );
    }

    updateTaskText(e) {
        e.preventDefault();
        this.taskText = e.target.value;
    }

    clickHandler() {
        this.props.callback(this.taskText);
    }
}

NewTask.propTypes = {
    callback: React.PropTypes.func
};

NewTask.defaultProps = {
    callback: () => console.log('callback was not defined')
};
