import React from 'react';
import {TasksView} from 'TasksView';
import {NewTask} from 'NewTask';
import {Modal} from 'react-bootstrap';
import {TaskModel} from 'TaskModel';

export class MainView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newTask: null
        };
    }

    render() {
        return (
            <div className = "static-modal">
                <Modal.Dialog>
                    <Modal.Body>
                        <TasksView newTask = {this.state.newTask}/>
                        <NewTask callback = {this.addNewTask.bind(this)}/>
                    </Modal.Body>
                </Modal.Dialog>
            </div>
        );
    }

    addNewTask(taskText) {
        this.setState({
            newTask: new TaskModel(taskText)
        });
    }
}
