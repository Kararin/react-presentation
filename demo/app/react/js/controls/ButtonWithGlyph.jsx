import React from 'react';
import {Button, Glyphicon} from 'react-bootstrap';

export class GlyphButton extends React.Component {
    render() {
        return (
             <Button bsStyle = {this.props.bsStyle}
                     onClick = {this.clickHandler.bind(this)}>
                    <Glyphicon glyph= {this.props.glyph} />
            </Button>
        );
    }

    clickHandler() {
        this.props.clickCallback();
    }
}

GlyphButton.propTypes = {
    bsStyle: React.PropTypes.string,
    glyph: React.PropTypes.string,
    clickCallback: React.PropTypes.func
}

GlyphButton.defaultProps = {
    bsStyle: 'default',
    glyph: '',
    clickCallback: () => console.log('clickCallback was not defined')
};
