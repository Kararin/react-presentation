import React from 'react';
import {TasksListView} from 'TasksListView';

export class TasksView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            doneTasks: [],
            toDoTasks: []
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState((state, props) => {
            state.toDoTasks.push(nextProps.newTask);
        });
    }

    render() {
        var todoProps = {
                tasks: this.state.toDoTasks,
                update: true,
                toDoneCallback: this.toDone.bind(this)
            },
            doneProps = {
                tasks: this.state.doneTasks
            };

        return (
            <div>
                <h2>
                    Tasks
                </h2>

                <h4>
                    ToDo:
                </h4>
                    <TasksListView {...todoProps}/>
                <h4>
                    Done:
                </h4>
                    <TasksListView {...doneProps}/>
            </div>
        );
    }

    updateTasks(changedTask) {
        this.setState((state, props) => {
            state.toDoTasks.splice(state.toDoTasks.indexOf(changedTask), 1);
            state.doneTasks.push(changedTask);
        });
    }

    toDone(task) {
        task.isDone = true;

        this.updateTasks(task);
    }

}

TasksView.propTypes = {
    tasks: React.PropTypes.array,
};

TasksView.defaultProps = {
    tasks: []
};