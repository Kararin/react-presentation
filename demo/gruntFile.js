module.exports = function(grunt) {

    grunt.initConfig({
        pkg: 'package.json',

        reactApp: 'app/react/',
        rlibs: 'app/react/lib/',
        vanillaApp: 'app/vanilla/',
        vlibs: 'app/vanilla/lib',
        temp: 'temp/',

        babel: {
            options: {
                sourceMap: true,
                presets: ['es2015', 'react']
            },
            files: {
                expand: true,
                cwd: '<%= reactApp%>',
                src: ['**/*.js', '**/*.jsx', '!lib/**/*'],
                dest: '<%= temp%>',
                ext: '.js'
            }
        },

        browserify: {
            '<%= vanillaApp%>/module.js': ['<%= temp%>**/*.js', '!<%= vlibs%>/**/*'],
            options: {
                sourceMap: true,
                alias: {
                    'TasksView': './<%= temp%>js/TasksView.js',
                    'MainView': './<%= temp%>js/MainView.js',
                    'NewTask': './<%= temp%>js/NewTaskView.js',
                    'TaskModel': './<%= temp%>js/model/TaskModel.js',
                    'OneTaskView': './<%= temp%>js/OneTaskView.js',
                    'TasksListView': './<%= temp%>js/TasksListView.js',
                    'InputView': './<%= temp%>js/controls/InputView.js',
                    'GlyphButton': './<%= temp%>js/controls/ButtonWithGlyph.js'
                }
            }
        },

        clean: {
            vanilla: {
                expand: true,
                src: '<%= vanillaApp%>'
            },

            temp: {
                expand: true,
                src: '<%= temp%>'
            }
        },

        copy: {
            lib: {
                files: [{
                    expand: true,
                    cwd: '<%= rlibs%>',
                    src: ['**/*'],
                    dest: '<%= vlibs%>'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-reactify');
    grunt.loadNpmTasks('grunt-browserify');

    grunt.registerTask('default', ['clean:vanilla', 'copy', 'clean:temp', 'babel', 'browserify', 'clean:temp']);
};
